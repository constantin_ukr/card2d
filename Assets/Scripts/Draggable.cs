﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class Draggable : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler
{
    private static HorizontalLayoutGroup _horizontalLayout;
    private Vector3 _startPosition;
    private Transform _transform;
    private GameInstance _game;
    private Card _card;
    private GraphicRaycaster _raycaster;
    private EventSystem _eventSystem;

    void Awake()
    {
        _game = GameObject.Find("GameManager").GetComponent<GameInstance>();
        _card = GetComponent<Card>();
        _transform = GetComponent<Transform>();
        _raycaster = GameObject.FindObjectOfType<GraphicRaycaster>();
        _eventSystem = GameObject.FindObjectOfType<EventSystem>();
    }

    void Start()
    {
        if (_horizontalLayout == null)
        {
            _horizontalLayout = GetComponentInParent<HorizontalLayoutGroup>();
        }
    }

    public void OnBeginDrag(PointerEventData eventData)
    {
        _startPosition = new Vector3(_transform.position.x, _transform.position.y, 0);
    }

    public void OnDrag(PointerEventData eventData)
    {
        transform.position = eventData.position;
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        if (IsOverCardPool("Vidbiy"))
        {
            Destroy(gameObject);
            var card = gameObject.GetComponent<Card>();
            _game.DropCardToPool(card);
        }
        else if (Mathf.Abs(gameObject.transform.position.y - _startPosition.y) > 10.0f)
        {
            Ray ry = Camera.main.ScreenPointToRay(new Vector3(Input.mousePosition.x, Input.mousePosition.y, 0));
            RaycastHit hit;
            if (Physics.Raycast(ry, out hit, 100))
            {
                var spriteRenderer = hit.collider.gameObject.GetComponent<SpriteRenderer>();
                if (spriteRenderer != null)
                {
                    var card = gameObject.GetComponent<Card>();
                    var posData = hit.collider.gameObject.name.Split(new char[] { '_' });
                    var pos = new Position2D(int.Parse(posData[1].Replace("r", "")), int.Parse(posData[2].Replace("c", "")));
                    print(pos.C + " " + pos.R);
#if UNITY_EDITOR
                    Logger.Write($"card Id: {card.Id} playing on position : R{pos.R}, C:{pos.C} {Environment.NewLine}");
#endif
                    if (!_game.PlayCardOnGameField(card, pos))
                    {
                        gameObject.transform.position = _startPosition;
                    }
                    else
                    {
                        var img = gameObject.GetComponent<Image>();
                        spriteRenderer.sprite = img.sprite;
                        spriteRenderer.transform.localScale = img.transform.localScale;
                        Destroy(gameObject);
                        if (_game.IsPositionCanBeWin(pos))
                        {
                            var openCardsData = _game.GetOpenFinishCard(pos);

                            for (int i = 0; i < openCardsData.Count; i++)
                            {
                                var itemName = string.Format("cell_r{0}_c{1}", openCardsData[i].Item2.R, openCardsData[i].Item2.C);
                                var go = GameObject.Find(itemName).GetComponent<SpriteRenderer>();
                                var tempCardData = CardPool.GetSpecialCardById(openCardsData[i].Item1) as SpecialCardBuilder;
                                if (tempCardData != null)
                                {
                                    var inst = Instantiate(Resources.Load<GameObject>(tempCardData.Name));
                                    var img2 = inst.GetComponent<SpriteRenderer>();
                                    
                                    go.sprite = img2.sprite;
                                    go.transform.localScale = tempCardData.CardRoteted ? new Vector3(-1, -1, 0) : Vector3.one;
                                    Destroy(inst);
                                }
                            }
                        }
                    }
                }
            }
        }
        else
        {
            gameObject.transform.position = _startPosition;
            print(gameObject.name + " " + gameObject.transform.position);
        }

        _card.Selected = false;
        _horizontalLayout.childForceExpandHeight = false;
        _horizontalLayout.childForceExpandHeight = true;
    }

    private bool IsOverCardPool(string id)
    {
        var pointerEventData = new PointerEventData(_eventSystem);
        pointerEventData.position = Input.mousePosition;
        var results = new List<RaycastResult>();
        _raycaster.Raycast(pointerEventData, results);
        foreach (var result in results)
        {
            if (result.gameObject.name == id)
            {
                return true;
            }
        }

        return false;
    }
}
