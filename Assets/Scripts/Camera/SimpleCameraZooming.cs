﻿using UnityEngine;

public class SimpleCameraZooming : MonoBehaviour
{
    [SerializeField]
    private float zoomSpeed = 2.0f;
    
    void Update ()
    {
        float scroll = Input.GetAxis("Mouse ScrollWheel");
        transform.Translate(0, scroll * zoomSpeed, scroll * zoomSpeed, Space.World);
    }
}
