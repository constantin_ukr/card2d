﻿using UnityEngine;
using UnityEngine.UI;

public class MouseOverUITracker : MonoBehaviour {

    private Image _img;
    private Color _defaultColor;
    private RectTransform _rectTransform;

    void Awake()
    {
        _img = GetComponent<Image>();
        _defaultColor = _img.color;
        _rectTransform = transform as RectTransform;
    }

    void Update()
    {
        if (RectTransformUtility.RectangleContainsScreenPoint(_rectTransform, Input.mousePosition))
        {
            _img.color = Color.red;
        }
        else
        {
            _img.color = _defaultColor;
        }
    }
}
