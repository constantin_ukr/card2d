﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class HandCardsManager : MonoBehaviour
{
    public GameObject Prefab;
    private static readonly List<GameObject> _cardInst = new List<GameObject>();
    private static readonly List<Card> _cards = new List<Card>();
    private static Card _selectedCard;
    private readonly Vector3 _rotateScale = new Vector3(-1, -1, 0);
    private GameObject _panel;

    private void Awake()
    {
        _panel = GameObject.Find("Panel");
    }

    void Start()
    {        
        for (int i = 0; i < Configuration.CardCountForPlayerOnStartGame; i++)
        {
            GetNewCard();
        }
    }    

    void Update()
    {
        if (Input.GetKeyUp(KeyCode.F))
        {
            foreach (var card in _cards.Where(crd => crd != null && crd.Selected))
            {
                if (card.Type == CardType.Builder)
                {
                    var cardBuilder = (CardBuilder)card.GetData();
                    card.transform.localScale = cardBuilder.CardRoteted ? Vector3.one : _rotateScale;
                    cardBuilder.RotateCard();
                }
            }
        }
    }

    public void SelectedCard(Card card)
    {
        if(_selectedCard != null && card.Id == _selectedCard.Id && _selectedCard.Selected)
        {
            return;
        }
        foreach (var c in _cards.Where(crd => crd != null))
        {
            c.Selected = false;
        }

        card.Selected = true;
        _selectedCard = card;
    }

    public void TakeNewCard()
    {
        GetNewCard();
    }

    private void GetNewCard()
    {
        var cardData = CardPool.GetRandomCard();
        var resource = Resources.Load<GameObject>(string.Format("card_{0}", cardData.Id));
        var cardObj = Instantiate(Prefab, gameObject.transform);
        cardObj.transform.SetParent(_panel.transform);
        cardObj.GetComponent<Image>().sprite = resource.GetComponent<Image>().sprite;
        var card = cardObj.GetComponent<Card>();
        card.SetCardData(cardData);
        _cardInst.Add(cardObj);
        _cards.Add(card);
    }
}
