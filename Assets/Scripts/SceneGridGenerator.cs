﻿using UnityEngine;
using UnityEngine.UI;

public class SceneGridGenerator : MonoBehaviour
{
    private int _width;
    private int _height;
    private Transform _envTransform;

    void Awake()
    {
        _width = Configuration.GameFiledWidth;
        _height = Configuration.GameFiledHeight;
        _envTransform = GameObject.Find("Env").transform;
        CreateCells();
        Vector2[] cellBacksideConteiners =
        {
            new Vector2(Configuration.StartPositionX, Configuration.StartPositionY),
            new Vector2(Configuration.StartPositionX + Configuration.OffsetToFinishPositionX, Configuration.StartPositionY - Configuration.OffsetTopFinishPositionY),
            new Vector2(Configuration.StartPositionX + Configuration.OffsetToFinishPositionX, Configuration.StartPositionY),
            new Vector2(Configuration.StartPositionX + Configuration.OffsetToFinishPositionX, Configuration.StartPositionY + Configuration.OffsetDownFinishPositionY)
        };

        SetStartItems(cellBacksideConteiners);
        PreparaCameraPosition(cellBacksideConteiners[0], cellBacksideConteiners[2]);
    }

    private void CreateCells()
    {
        GameObject initCell = Instantiate(Resources.Load<GameObject>("Cell"));
        var sprteRenderSize = initCell.GetComponent<SpriteRenderer>().size;
        float spriteWidth = sprteRenderSize.x;
        float spriteHeight = sprteRenderSize.y;
        Destroy(initCell);

        var offsetX = 0f;
        var offsetY = 0f;

        for (int row = 0; row < _height; row++)
        {
            for (int col = 0; col < _width; col++)
            {
                GameObject cell = Instantiate(Resources.Load<GameObject>("Cell"));
                cell.transform.position = new Vector3(offsetX, offsetY);
                offsetX += spriteWidth + 0.01f;

                cell.name = string.Format("cell_r{0}_c{1}", row, col);
                cell.transform.SetParent(_envTransform);
            }

            offsetY += -spriteHeight;
            offsetX = 0;
        }
    }

    private void SetStartItems(Vector2[] cellBacksideConteiners)
    {
        GameObject[] backsides = new GameObject[4];

        for (int i = 0; i < backsides.Length; i++)
        {
            string prefabName = (i == 0) ? "start" : "backside";
            
            backsides[i] = Instantiate(Resources.Load<GameObject>(prefabName));
            GameObject container = GameObject.Find(string.Format("cell_r{0}_c{1}", cellBacksideConteiners[i].y, cellBacksideConteiners[i].x));
            var img = backsides[i].GetComponent<SpriteRenderer>();
            var spriteRenderer = container.GetComponent<SpriteRenderer>();
            spriteRenderer.sprite = img.sprite;
            Destroy(backsides[i]);
        }
    }

    private void PreparaCameraPosition(Vector2 targetStartCoord, Vector2 targetFinishCoord)
    {
        Camera camera = Camera.main;

        GameObject[] target = new GameObject[2];

        target[0] = GameObject.Find(string.Format("cell_r{0}_c{1}", targetStartCoord.y, targetStartCoord.x));
        target[1] = GameObject.Find(string.Format("cell_r{0}_c{1}", targetFinishCoord.y, targetFinishCoord.x));

        float x = (target[0].transform.position.x + target[1].transform.position.x) / 2f;
        float y = (target[0].transform.position.y + target[1].transform.position.y) / 2f;

        camera.transform.position = new Vector3(x, y, camera.transform.position.z);
    }
}
