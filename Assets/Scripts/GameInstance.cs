﻿using System;
using System.Collections.Generic;
using UnityEngine;

class GameInstance : MonoBehaviour
{
    private bool _isGameRun = false;
    private HandCardsManager _handCardsManager;
    private RectTransform[] _otherPlayerHolderes;
    
    void Awake()
    {
        _handCardsManager = GetComponent<HandCardsManager>();
        StartGame();
    }

    public void StartGame()
    {
        if (_isGameRun)
        {
            return;
        }
#if UNITY_EDITOR
        Logger.Create(@"D:\log.txt", true);
#endif
        CardPool.GenerateCardPool();
        
        GameFieldMatrix.CreateMatrix(
            Configuration.GameFiledHeight,
            Configuration.GameFiledWidth,
            new Position2D(Configuration.StartPositionY, Configuration.StartPositionX),
            Configuration.OffsetToFinishPositionX,
            Configuration.OffsetTopFinishPositionY,
            Configuration.OffsetDownFinishPositionY);
        
        _isGameRun = true;
    }

#if UNITY_EDITOR
    private void Start()
    {
        
        GameFieldMatrix.PrintMap();
    }
#endif
    
    public bool PlayCardOnGameField(Card card, Position2D targetPosition)
    {
        switch (card.Type)
        {
            case CardType.Builder:
                CardBuilder cardBuilder = (CardBuilder)card.GetData();
                if (GameFieldMatrix.PlayCardByPosition(cardBuilder.Vertexes, cardBuilder.Edges, targetPosition))
                {
                    _handCardsManager.TakeNewCard();
                    return true;
                }
                else
                {
                    return false;
                }
            case CardType.Landslide:
                if (GameFieldMatrix.PlayCardByLandslide(targetPosition))
                {
                    _handCardsManager.TakeNewCard();

                    return true;
                }
                else
                {
                    return false;
                }
            case CardType.Locker:
                break;
            case CardType.Unlocker:
                break;
            case CardType.Start:
                break;
            case CardType.Background:
                break;
            case CardType.Gold:
                break;
            case CardType.Coal:
                break;
        }

        return false;
    }

    public bool IsPositionCanBeWin(Position2D position)
    {
        return GameFieldMatrix.IsPositionCanBeWin(position);
    }

    public List<Tuple<int, Position2D>> GetOpenFinishCard(Position2D position)
    {
        var result = GameFieldMatrix.OpenFinishCardsForPosition(position);
        return result;
    }

    public void DropCardToPool(Card card)
    {
        _handCardsManager.TakeNewCard();
    }

    /*public static bool PlayCardToLockOtherPlayer(CardLocker card, int otherPlayerId)
    {
        CardType cardType = card.GeCardType();

        if (cardType == CardType.Locker)
        {
            Console.WriteLine("Locker");
            return true;
        }
        else
        {
            Console.WriteLine("Not valid type");
            return false;
        }
    }*/

    /*public static bool PlayCardToUnlockOtherPlayer(CardUnlocker card, int otherPlayerId, LockerUnlockerType concreteType = LockerUnlockerType.None)
    {
        Player otherPlayer = FindPlayerById(otherPlayerId);
        List<LockerUnlockerType> allLokers = otherPlayer.GetAllLockers();

        if (allLokers.Count == 0)
        {
            Console.WriteLine("Player haven't any lockers");
            return false;
        }

        List<LockerUnlockerType> allUnlokcres = card.GetAllUnlockerTypes();

        if (concreteType != LockerUnlockerType.None)
        {
            if (allLokers.Contains(concreteType))
            {
                Console.WriteLine("Unlocker");

                return true;
            }
            else
            {
                //Console.WriteLine("Player haven't {0} locker", concreteType);
                return false;
            }
        }

        for (int i = 0; i < allUnlokcres.Count; i++)
        {
            if (allLokers.Contains(allUnlokcres[i]))
            {
                Console.WriteLine("Unlocker");

                otherPlayer.DoUnlock(allUnlokcres[i]);

                return true;
            }
        }

        return false;
    }*/
}
