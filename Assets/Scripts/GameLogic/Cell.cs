﻿public class Cell
{
    public bool IsEmpty { get; set; }
    public Position2D Position { get; set; }

    public Vertex[] Vertices { get; set; }
    public Edge[] Edges { get; set; }

    public byte Mask { get; set; }

    public Cell()
    {
        IsEmpty = true;
        Position = new Position2D();
        Vertices = new Vertex[]
        {
                new Vertex(PositionType.Top),
                new Vertex(PositionType.Bottom),
                new Vertex(PositionType.Left),
                new Vertex(PositionType.Right)
        };
    }
}
