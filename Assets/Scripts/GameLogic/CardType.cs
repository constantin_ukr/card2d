﻿public enum CardType : byte
{
    Builder,
    Locker,
    Unlocker,
    Start,
    Background,
    Gold,
    Coal,
    Landslide
}
