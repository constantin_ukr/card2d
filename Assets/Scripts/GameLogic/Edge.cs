﻿public class Edge
{
    public int Id { get; set; }
    public Vertex VertexOneId { get; set; }
    public Vertex VertexTwoId { get; set; }

    public Edge(Vertex vertexOne, Vertex vertexTwo)
    {
        VertexOneId = vertexOne;
        VertexTwoId = vertexTwo;
    }
}
