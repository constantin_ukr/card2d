﻿public static class Configuration
{
    #region GameStartConfigure
    public static byte GameFiledWidth = 11;
    public static byte GameFiledHeight = 7;
     
    public static byte StartPositionX = 2;
    public static byte StartPositionY = 3;
     
    public static byte OffsetToFinishPositionX = 3;
    public static byte OffsetTopFinishPositionY = 2;
    public static byte OffsetDownFinishPositionY = 2;

    public static byte CardCountForPlayerOnStartGame = 15;

    public static byte StartPositionCount = 1;
    public static byte FinishPositionCount = 3;

    #endregion
}