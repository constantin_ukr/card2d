﻿using System;
using System.Collections.Generic;
using System.Linq;

static class CardPool
{
    static List<CardData> _cards = new List<CardData>();
    static List<CardData> _specialCards = new List<CardData>();

    public static void GenerateCardPool()
    {
        CreateBlockerCrads();
        CreateBuilderCrads();
        CreateLandslideCrads();
        //AddIdToCard();
        
        CreateFinishCards();
        _specialCards.Shuffle();
    }

    public static CardData GetRandomCard()
    {
        int index = new Random().Next(0, _cards.Count - 1);
        CardData card = _cards[index];
        
        _cards.RemoveAt(index);

        return card;
    }

    public static CardData GetSpecialCardByIndex(int index)
    {
        if (index < 0 || index > _specialCards.Count - 1)
        {
            throw new IndexOutOfRangeException("WTF");
        }

        var data = _specialCards[index];
        
        return data;
    }
    
    public static CardData GetSpecialCardById(int id, bool shouldRemove = true)
    {
        var result = _specialCards.First(card => card.Id == id);
        if (shouldRemove)
        {
            _specialCards.Remove(result);
        }

        return result;
    }

    /*public static CardData TEMPGetCardById(int id)
    {
        return _cards.First(c => c.Id == id);
    }*/

    private static void CreateFinishCards()
    {
        List<Vertex> goldVertices = new List<Vertex>
        {
            new Vertex(PositionType.Top){IsActive = true},
            new Vertex(PositionType.Right){IsActive = true},
            new Vertex(PositionType.Bottom){IsActive = true},
            new Vertex(PositionType.Left){IsActive = true}
        };

        List<Edge> goldEdges = new List<Edge>()
        {
            new Edge(goldVertices[0], goldVertices[1]),
            new Edge(goldVertices[1], goldVertices[2]),
            new Edge(goldVertices[2], goldVertices[3]),
            new Edge(goldVertices[3], goldVertices[0])
        };

        var goldCard = new SpecialCardBuilder("gold");
        goldCard.Vertexes = goldVertices.ToArray();
        goldCard.Edges = goldEdges.ToArray();
        goldCard.Id = 499;
        goldCard.Type = CardType.Gold;
        _specialCards.Add(goldCard);

        List<Vertex> verticesBR = new List<Vertex>
        {
            new Vertex(PositionType.Top){IsActive = false},
            new Vertex(PositionType.Right){IsActive = true},
            new Vertex(PositionType.Bottom){IsActive = true},
            new Vertex(PositionType.Left){IsActive = false}
        };

        List<Edge> edgesBR = new List<Edge>()
        {
            new Edge(verticesBR[1], verticesBR[2])
        };

        var coalBR = new SpecialCardBuilder("coal_0");
        coalBR.Id = 497;
        coalBR.Vertexes = verticesBR.ToArray();
        coalBR.Edges = edgesBR.ToArray();
        coalBR.Type = CardType.Coal;
        _specialCards.Add(coalBR);
        
        List<Vertex> verticesBL = new List<Vertex>
        {
            new Vertex(PositionType.Top){IsActive = false},
            new Vertex(PositionType.Right){IsActive = false},
            new Vertex(PositionType.Bottom){IsActive = true},
            new Vertex(PositionType.Left){IsActive = true}
        };

        List<Edge> edgesBL = new List<Edge>()
        {
            new Edge(verticesBL[2], verticesBL[3])
        };

        var coalBL = new SpecialCardBuilder("coal_1");
        coalBL.Id = 498;
        coalBL.Vertexes = verticesBL.ToArray();
        coalBL.Edges = edgesBL.ToArray();
        coalBL.Type = CardType.Coal;
        _specialCards.Add(coalBL);
    }
    
    private static void CreateBlockerCrads()
        {
            List<Vertex> vertexesBR = new List<Vertex>
            {
                new Vertex(PositionType.Top){IsActive = false},
                new Vertex(PositionType.Right){IsActive = true},
                new Vertex(PositionType.Bottom){IsActive = true},
                new Vertex(PositionType.Left){IsActive = false}
            };

            var blockerBR = new CardBuilder();
            blockerBR.Vertexes = vertexesBR.ToArray();
            blockerBR.Edges = new Edge[0];
            blockerBR.Id = 0;
            _cards.Add(blockerBR);

            List<Vertex> vertexesBL = new List<Vertex>
            {
                new Vertex(PositionType.Top){IsActive = false},
                new Vertex(PositionType.Right){IsActive = false},
                new Vertex(PositionType.Bottom){IsActive = true},
                new Vertex(PositionType.Left){IsActive = true}
            };

            var blockerBL = new CardBuilder();
            blockerBL.Id = 1;
            blockerBL.Vertexes = vertexesBL.ToArray();
            blockerBL.Edges = new Edge[0];
            _cards.Add(blockerBL);

            List<Vertex> vertexesTB = new List<Vertex>
            {
                new Vertex(PositionType.Top){IsActive = true},
                new Vertex(PositionType.Right){IsActive = false},
                new Vertex(PositionType.Bottom){IsActive = true},
                new Vertex(PositionType.Left){IsActive = false}
            };

            var blockerTB = new CardBuilder();
            blockerTB.Id = 2;
            blockerTB.Vertexes = vertexesTB.ToArray();
            blockerTB.Edges = new Edge[0];
            _cards.Add(blockerTB);

            List<Vertex> vertexesR = new List<Vertex>
            {
                new Vertex(PositionType.Top){IsActive = false},
                new Vertex(PositionType.Right){IsActive = true},
                new Vertex(PositionType.Bottom){IsActive = false},
                new Vertex(PositionType.Left){IsActive = false}
            };

            var blockerR = new CardBuilder();
            blockerR.Id = 3;
            blockerR.Vertexes = vertexesR.ToArray();
            blockerR.Edges = new Edge[0];
            _cards.Add(blockerR);

            List<Vertex> vertexesTBR = new List<Vertex>
            {
                new Vertex(PositionType.Top){IsActive = true},
                new Vertex(PositionType.Right){IsActive = true},
                new Vertex(PositionType.Bottom){IsActive = true},
                new Vertex(PositionType.Left){IsActive = false}
            };

            var blockerTBR = new CardBuilder();
            blockerTBR.Id = 4;
            blockerTBR.Vertexes = vertexesTBR.ToArray();
            blockerTBR.Edges = new Edge[0];
            _cards.Add(blockerTBR);

            List<Vertex> vertexesTBRL = new List<Vertex>
            {
                new Vertex(PositionType.Top){IsActive = true},
                new Vertex(PositionType.Right){IsActive = true},
                new Vertex(PositionType.Bottom){IsActive = true},
                new Vertex(PositionType.Left){IsActive = true}
            };

            var blockerTBRL = new CardBuilder();
            blockerTBRL.Id = 5;
            blockerTBRL.Vertexes = vertexesTBRL.ToArray();
            blockerTBRL.Edges = new Edge[0];
            _cards.Add(blockerTBRL);

            var blockerTBRL6 = new CardBuilder();
            blockerTBRL6.Id = 6;
            blockerTBRL6.Vertexes = vertexesTBRL.ToArray();
            blockerTBRL6.Edges = new Edge[0];
            _cards.Add(blockerTBRL6);

            List<Vertex> vertexesRL = new List<Vertex>
            {
                new Vertex(PositionType.Top){IsActive = false},
                new Vertex(PositionType.Right){IsActive = true},
                new Vertex(PositionType.Bottom){IsActive = false},
                new Vertex(PositionType.Left){IsActive = true}
            };

            var blockerRL = new CardBuilder();
            blockerRL.Id = 7;
            blockerRL.Vertexes = vertexesRL.ToArray();
            blockerRL.Edges = new Edge[0];
            _cards.Add(blockerRL);

            List<Vertex> vertexesT = new List<Vertex>
            {
                new Vertex(PositionType.Top){IsActive = true},
                new Vertex(PositionType.Right){IsActive = false},
                new Vertex(PositionType.Bottom){IsActive = false},
                new Vertex(PositionType.Left){IsActive = false}
            };

            var blockerT = new CardBuilder();
            blockerT.Id = 8;
            blockerT.Vertexes = vertexesT.ToArray();
            blockerT.Edges = new Edge[0];
            _cards.Add(blockerT);


            List<Vertex> vertexesTRL = new List<Vertex>
            {
                new Vertex(PositionType.Top){IsActive = true},
                new Vertex(PositionType.Right){IsActive = true},
                new Vertex(PositionType.Bottom){IsActive = false},
                new Vertex(PositionType.Left){IsActive = true}
            };

            var blockerTRL = new CardBuilder();
            blockerTRL.Id = 9;
            blockerTRL.Vertexes = vertexesTRL.ToArray();
            blockerTRL.Edges = new Edge[0];
            _cards.Add(blockerTRL);
        }

        private static void CreateBuilderCrads()
        {
            List<Vertex> vertexesRL = new List<Vertex>
            {
                new Vertex(PositionType.Top){IsActive = false},
                new Vertex(PositionType.Right){IsActive = true},
                new Vertex(PositionType.Bottom){IsActive = false},
                new Vertex(PositionType.Left){IsActive = true}
            };

            List<Edge> edgesRL = new List<Edge>()
            {
                new Edge(vertexesRL[1], vertexesRL[3])
            };

            var builderRL = new CardBuilder();
            builderRL.Id = 10;
            builderRL.Vertexes = vertexesRL.ToArray();
            builderRL.Edges = edgesRL.ToArray();
            _cards.Add(builderRL);

            var builderRL11 = new CardBuilder();
            builderRL11.Id = 11;
            builderRL11.Vertexes = vertexesRL.ToArray();
            builderRL11.Edges = edgesRL.ToArray();
            _cards.Add(builderRL11);

            var builderRL12 = new CardBuilder();
            builderRL12.Id = 12;
            builderRL12.Vertexes = vertexesRL.ToArray();
            builderRL12.Edges = edgesRL.ToArray();
            _cards.Add(builderRL12);

            var builderRL13 = new CardBuilder();
            builderRL13.Id = 13;
            builderRL13.Vertexes = vertexesRL.ToArray();
            builderRL13.Edges = edgesRL.ToArray();
            _cards.Add(builderRL13);

            var builderRL14 = new CardBuilder();
            builderRL14.Id = 14;
            builderRL14.Vertexes = vertexesRL.ToArray();
            builderRL14.Edges = edgesRL.ToArray();
            _cards.Add(builderRL14);

            //------------------------------------------

            List<Vertex> vertexesBRL = new List<Vertex>
            {
                new Vertex(PositionType.Top){IsActive = false},
                new Vertex(PositionType.Right){IsActive = true},
                new Vertex(PositionType.Bottom){IsActive = true},
                new Vertex(PositionType.Left){IsActive = true}
            };

            List<Edge> edgesBRL = new List<Edge>()
            {
                new Edge(vertexesBRL[2], vertexesBRL[1]),
                new Edge(vertexesBRL[1], vertexesBRL[3]),
                new Edge(vertexesBRL[3], vertexesBRL[2])
            };

            var builderBRL = new CardBuilder();
            builderBRL.Id = 15;
            builderBRL.Vertexes = vertexesBRL.ToArray();
            builderBRL.Edges = edgesBRL.ToArray();
            _cards.Add(builderBRL);

            var builderBRL16 = new CardBuilder();
            builderBRL16.Id = 16;
            builderBRL16.Vertexes = vertexesBRL.ToArray();
            builderBRL16.Edges = edgesBRL.ToArray();
            _cards.Add(builderBRL16);

            var builderBRL17 = new CardBuilder();
            builderBRL17.Id = 17;
            builderBRL17.Vertexes = vertexesBRL.ToArray();
            builderBRL17.Edges = edgesBRL.ToArray();
            _cards.Add(builderBRL17);

            var builderBRL18 = new CardBuilder();
            builderBRL18.Id = 18;
            builderBRL18.Vertexes = vertexesBRL.ToArray();
            builderBRL18.Edges = edgesBRL.ToArray();
            _cards.Add(builderBRL18);

            var builderBRL19 = new CardBuilder();
            builderBRL19.Id = 19;
            builderBRL19.Vertexes = vertexesBRL.ToArray();
            builderBRL19.Edges = edgesBRL.ToArray();
            _cards.Add(builderBRL19);

            //------------------------------------------

            List<Vertex> vertexesBR = new List<Vertex>
            {
                new Vertex(PositionType.Top){IsActive = false},
                new Vertex(PositionType.Right){IsActive = true},
                new Vertex(PositionType.Bottom){IsActive = true},
                new Vertex(PositionType.Left){IsActive = false}
            };

            List<Edge> edgesBR = new List<Edge>()
            {
                new Edge(vertexesBR[2], vertexesBR[1])
            };

            var builderBR = new CardBuilder();
            builderBR.Id = 20;
            builderBR.Vertexes = vertexesBR.ToArray();
            builderBR.Edges = edgesBR.ToArray();
            _cards.Add(builderBR);

            var builderBR21 = new CardBuilder();
            builderBR21.Id = 21;
            builderBR21.Vertexes = vertexesBR.ToArray();
            builderBR21.Edges = edgesBR.ToArray();
            _cards.Add(builderBR21);

            var builderBR22 = new CardBuilder();
            builderBR22.Id = 22;
            builderBR22.Vertexes = vertexesBR.ToArray();
            builderBR22.Edges = edgesBR.ToArray();
            _cards.Add(builderBR22);

            var builderBR23 = new CardBuilder();
            builderBR23.Id = 23;
            builderBR23.Vertexes = vertexesBR.ToArray();
            builderBR23.Edges = edgesBR.ToArray();
            _cards.Add(builderBR23);

            var builderBR24 = new CardBuilder();
            builderBR24.Id = 24;
            builderBR24.Vertexes = vertexesBR.ToArray();
            builderBR24.Edges = edgesBR.ToArray();
            _cards.Add(builderBR24);

            //------------------------------------------

            List<Vertex> vertexesBL = new List<Vertex>
            {
                new Vertex(PositionType.Top){IsActive = false},
                new Vertex(PositionType.Right){IsActive = false},
                new Vertex(PositionType.Bottom){IsActive = true},
                new Vertex(PositionType.Left){IsActive = true}
            };

            List<Edge> edgesBL = new List<Edge>()
            {
                new Edge(vertexesBL[2], vertexesBL[3])
            };

            var builderBL = new CardBuilder();
            builderBL.Id = 25;
            builderBL.Vertexes = vertexesBL.ToArray();
            builderBL.Edges = edgesBL.ToArray();
            _cards.Add(builderBL);

            var builderBL26 = new CardBuilder();
            builderBL26.Id = 26;
            builderBL26.Vertexes = vertexesBL.ToArray();
            builderBL26.Edges = edgesBL.ToArray();
            _cards.Add(builderBL26);

            var builderBL27 = new CardBuilder();
            builderBL27.Id = 27;
            builderBL27.Vertexes = vertexesBL.ToArray();
            builderBL27.Edges = edgesBL.ToArray();
            _cards.Add(builderBL27);

            var builderBL28 = new CardBuilder();
            builderBL28.Id = 28;
            builderBL28.Vertexes = vertexesBL.ToArray();
            builderBL28.Edges = edgesBL.ToArray();
            _cards.Add(builderBL28);

            var builderBL29 = new CardBuilder();
            builderBL29.Id = 29;
            builderBL29.Vertexes = vertexesBL.ToArray();
            builderBL29.Edges = edgesBL.ToArray();
            _cards.Add(builderBL29);

            //------------------------------------------

            List<Vertex> vertexesTB = new List<Vertex>
            {
                new Vertex(PositionType.Top){IsActive = true},
                new Vertex(PositionType.Right){IsActive = false},
                new Vertex(PositionType.Bottom){IsActive = true},
                new Vertex(PositionType.Left){IsActive = false}
            };

            List<Edge> edgesTB = new List<Edge>()
            {
                new Edge(vertexesTB[0], vertexesTB[2])
            };

            var builderTB = new CardBuilder();
            builderTB.Id = 30;
            builderTB.Vertexes = vertexesTB.ToArray();
            builderTB.Edges = edgesTB.ToArray();
            _cards.Add(builderTB);

            var builderTB31 = new CardBuilder();
            builderTB31.Id = 31;
            builderTB31.Vertexes = vertexesTB.ToArray();
            builderTB31.Edges = edgesTB.ToArray();
            _cards.Add(builderTB31);

            var builderTB32 = new CardBuilder();
            builderTB32.Id = 32;
            builderTB32.Vertexes = vertexesTB.ToArray();
            builderTB32.Edges = edgesTB.ToArray();
            _cards.Add(builderTB32);

            var builderTB33 = new CardBuilder();
            builderTB33.Id = 33;
            builderTB33.Vertexes = vertexesTB.ToArray();
            builderTB33.Edges = edgesTB.ToArray();
            _cards.Add(builderTB33);

            var builderTB34 = new CardBuilder();
            builderTB34.Id = 34;
            builderTB34.Vertexes = vertexesTB.ToArray();
            builderTB34.Edges = edgesTB.ToArray();
            _cards.Add(builderTB34);

            //------------------------------------------

            List<Vertex> vertexesTBL = new List<Vertex>
            {
                new Vertex(PositionType.Top){IsActive = true},
                new Vertex(PositionType.Right){IsActive = false},
                new Vertex(PositionType.Bottom){IsActive = true},
                new Vertex(PositionType.Left){IsActive = true}
            };

            List<Edge> edgesTBL = new List<Edge>()
            {
                new Edge(vertexesTBL[0], vertexesTBL[2]),
                new Edge(vertexesTBL[2], vertexesTBL[3]),
                new Edge(vertexesTBL[3], vertexesTBL[0])
            };

            var builderTBL = new CardBuilder();
            builderTBL.Id = 35;
            builderTBL.Vertexes = vertexesTBL.ToArray();
            builderTBL.Edges = edgesTBL.ToArray();
            _cards.Add(builderTBL);

            var builderTBL36 = new CardBuilder();
            builderTBL36.Id = 36;
            builderTBL36.Vertexes = vertexesTBL.ToArray();
            builderTBL36.Edges = edgesTBL.ToArray();
            _cards.Add(builderTBL36);

            var builderTBL37 = new CardBuilder();
            builderTBL37.Id = 37;
            builderTBL37.Vertexes = vertexesTBL.ToArray();
            builderTBL37.Edges = edgesTBL.ToArray();
            _cards.Add(builderTBL37);

            var builderTBL38 = new CardBuilder();
            builderTBL38.Id = 38;
            builderTBL38.Vertexes = vertexesTBL.ToArray();
            builderTBL38.Edges = edgesTBL.ToArray();
            _cards.Add(builderTBL38);

            var builderTBL39 = new CardBuilder();
            builderTBL39.Id = 39;
            builderTBL39.Vertexes = vertexesTBL.ToArray();
            builderTBL39.Edges = edgesTBL.ToArray();
            _cards.Add(builderTBL39);

            //------------------------------------------

            List<Vertex> vertexesTBRL = new List<Vertex>
            {
                new Vertex(PositionType.Top){IsActive = true},
                new Vertex(PositionType.Right){IsActive = true},
                new Vertex(PositionType.Bottom){IsActive = true},
                new Vertex(PositionType.Left){IsActive = true}
            };

            List<Edge> edgesTBRL = new List<Edge>()
            {
                new Edge(vertexesTBRL[0], vertexesTBRL[1]),
                new Edge(vertexesTBRL[1], vertexesTBRL[2]),
                new Edge(vertexesTBRL[2], vertexesTBRL[3]),
                new Edge(vertexesTBRL[3], vertexesTBRL[0])
            };

            var builderTBRL40 = new CardBuilder();
            builderTBRL40.Id = 40;
            builderTBRL40.Vertexes = vertexesTBRL.ToArray();
            builderTBRL40.Edges = edgesTBRL.ToArray();
            _cards.Add(builderTBRL40);

            var builderTBRL = new CardBuilder();
            builderTBRL.Id = 41;
            builderTBRL.Vertexes = vertexesTBRL.ToArray();
            builderTBRL.Edges = edgesTBRL.ToArray();
            _cards.Add(builderTBRL);

            var builderTBRL42 = new CardBuilder();
            builderTBRL42.Id = 42;
            builderTBRL42.Vertexes = vertexesTBRL.ToArray();
            builderTBRL42.Edges = edgesTBRL.ToArray();
            _cards.Add(builderTBRL42);

            var builderTBRL43 = new CardBuilder();
            builderTBRL43.Id = 43;
            builderTBRL43.Vertexes = vertexesTBRL.ToArray();
            builderTBRL43.Edges = edgesTBRL.ToArray();
            _cards.Add(builderTBRL43);

            var builderTBRL44 = new CardBuilder();
            builderTBRL44.Id = 44;
            builderTBRL44.Vertexes = vertexesTBRL.ToArray();
            builderTBRL44.Edges = edgesTBRL.ToArray();
            _cards.Add(builderTBRL44);
        }

    private static void CreateLandslideCrads()
    {
        _cards.Add(new CardLandslide { Id = 50, Type = CardType.Landslide});
        _cards.Add(new CardLandslide { Id = 51, Type = CardType.Landslide});
        _cards.Add(new CardLandslide { Id = 52, Type = CardType.Landslide});
    }

    /*private void AddIdToCard()
    {
        for (int i = 0; i < _cards.Count; i++)
        {
            _cards[i].Id = i;
        }
    }*/
}

