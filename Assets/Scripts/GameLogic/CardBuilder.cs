using System;
using System.Collections.Generic;
using System.Linq;

public class CardBuilder : CardData
{
    private bool _isCardRoteted = false;

    public Vertex[] Vertexes { get; set; }
    public Edge[] Edges { get; set; }

    public bool CardRoteted
    {
        get { return _isCardRoteted; }
    }

    public void RotateCard()
    {
        if (Vertexes.Length > 0)
        {
            List<PositionType> visited = new List<PositionType>();

            for (int i = 0; i < Vertexes.Length/2; i++)
            {
                if (!visited.Contains(Vertexes[i].PositionType))
                {
                    object[] oppositeVertexData = OppositeSideVertex(Vertexes[i]);

                    visited.Add(Vertexes[i].PositionType);

                    Vertex temp = new Vertex(Vertexes[i].PositionType)
                    {
                        IsActive = Vertexes[i].IsActive
                    };
                    Vertex oppoditeVertexData = ((Vertex) oppositeVertexData[1]);

                    Vertexes[i].IsActive = oppoditeVertexData.IsActive;

                    Vertexes[Convert.ToInt32(oppositeVertexData[0])].IsActive = temp.IsActive;

                    visited.Add(GetOppositeSide(Vertexes[i].PositionType));
                }
            }

            var vertexTop = Vertexes.First(v => v.PositionType == PositionType.Top);
            var vertexBottom = Vertexes.First(v => v.PositionType == PositionType.Bottom);
            var vertexLeft = Vertexes.First(v => v.PositionType == PositionType.Left);
            var vertexRight = Vertexes.First(v => v.PositionType == PositionType.Right);
            
            for (int i = 0; i < Edges.Length; i++)
            {
                bool foundFirst = false;
                bool foundSecond = false;
                if (Edges[i].VertexOneId.PositionType == PositionType.Top)
                {
                    Edges[i].VertexOneId = vertexBottom;
                    foundFirst = true;
                }
                else if (Edges[i].VertexTwoId.PositionType == PositionType.Top)
                {
                    Edges[i].VertexTwoId = vertexBottom;
                    foundSecond = true;
                }
                
                if (Edges[i].VertexOneId.PositionType == PositionType.Bottom && !foundFirst)
                {
                    Edges[i].VertexOneId = vertexTop;
                    foundFirst = true;
                }
                else if (Edges[i].VertexTwoId.PositionType == PositionType.Bottom && !foundSecond)
                {
                    Edges[i].VertexTwoId = vertexTop;
                    foundSecond = true;
                }
                
                if (Edges[i].VertexOneId.PositionType == PositionType.Left && !foundFirst)
                {
                    Edges[i].VertexOneId = vertexRight;
                    foundFirst = true;
                }
                else if (Edges[i].VertexTwoId.PositionType == PositionType.Left && !foundSecond)
                {
                    Edges[i].VertexTwoId = vertexRight;
                    foundSecond = true;
                }
                
                if (Edges[i].VertexOneId.PositionType == PositionType.Right && !foundFirst)
                {
                    Edges[i].VertexOneId = vertexLeft;
                    foundFirst = true;
                }
                else if (Edges[i].VertexTwoId.PositionType == PositionType.Right && !foundSecond)
                {
                    Edges[i].VertexTwoId = vertexLeft;
                    foundSecond = true;
                }
            }
        }

        _isCardRoteted = !_isCardRoteted;
    }

    private object[] OppositeSideVertex(Vertex currentVertex)
    {
        PositionType oppositeType = GetOppositeSide(currentVertex.PositionType);

        for (int i = 0; i < Vertexes.Length; i++)
        {
            if (Vertexes[i].PositionType == oppositeType)
            {
                return new object[]{i, Vertexes[i]};
            }
        }

        return null;
    }

    private PositionType GetOppositeSide(PositionType currPositionType)
    {
        switch (currPositionType)
        {
            case PositionType.Top:
                return PositionType.Bottom;
            case PositionType.Bottom:
                return PositionType.Top;
            case PositionType.Left:
                return PositionType.Right;
            case PositionType.Right:
                return PositionType.Left;
            default:
                throw new ArgumentOutOfRangeException();
        }
    }
}