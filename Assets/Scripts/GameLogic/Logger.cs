﻿using System.IO;
using System.Text;

public static class Logger
{
	private static string _pathToLog;
	

	public static void Create(string path, bool shouldCreateNew)
	{
		_pathToLog = path;
		if (shouldCreateNew && File.Exists(path))
		{
			File.Delete(path);
		}
		
		File.Create(path);
	}

	public static void Write(string text)
	{
		File.AppendAllText(_pathToLog, text, Encoding.UTF8);
	}

}
