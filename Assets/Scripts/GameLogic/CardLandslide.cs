﻿public class CardLandslide : CardData
{
    public readonly Vertex[] Vertexes = new Vertex[]
    {
        new Vertex(PositionType.Top) {IsActive = false},
        new Vertex(PositionType.Right) {IsActive = false},
        new Vertex(PositionType.Bottom) {IsActive = false},
        new Vertex(PositionType.Left) {IsActive = false}
    };
    
    public readonly Edge[] Edges = new Edge[0];
}
