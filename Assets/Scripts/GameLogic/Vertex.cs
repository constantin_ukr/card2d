﻿using System;

public class Vertex : IComparable<Vertex>
{
    public bool IsActive { get; set; }

    public PositionType PositionType { get; }

    public Vertex(PositionType positionType)
    {
        PositionType = positionType;
        IsActive = false;
    }

    public int CompareTo(Vertex other)
    {
        if (other == null)
        {
            throw new ArgumentNullException();
        }

        if (other.IsActive == IsActive && other.PositionType == PositionType)
        {
            return 0;
        }
        else
        {
            return -1;
        }
    }

    public override bool Equals(object obj)
    {
        var vertex = obj as Vertex;
        if (vertex == null)
        {
            return false;
        }

        return CompareTo(vertex) == 0;
    }
}
