﻿public class SpecialCardBuilder : CardBuilder
    {
        private string _spriteName;

        public SpecialCardBuilder(string spriteName)
        {
            if (string.IsNullOrWhiteSpace(spriteName))
            {
                throw new System.ArgumentNullException();
            }

            _spriteName = spriteName;
        }
        
        public string Name { get { return _spriteName; } }
    }
