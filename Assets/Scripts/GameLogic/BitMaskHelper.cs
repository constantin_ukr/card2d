﻿using System;
using System.Collections;
using System.Linq;

public static class BitMaskHelper
{
    public static byte ToBitMask(byte paramIndex)
    {
        return Convert.ToByte(1 << paramIndex);
    }

    public static ulong ManyToBitMask(params byte[] paramIndexes)
    {
        if (paramIndexes == null || paramIndexes.Length == 0)
        {
            throw new InvalidOperationException("Array 'paramIndexes' couldn't be null or empty.");
        }

        byte mask = 0;
        for (int i = 0; i < paramIndexes.Length; i++)
        {
            AddFlagToMask(ref mask, ToBitMask(paramIndexes[i]));
        }

        return mask;
    }

    public static void AddFlagToMask(ref byte mask, byte flag)
    {
        mask |= flag;
    }

    public static void RemoveFlagFromMask(ref byte mask, byte flag)
    {
        mask &= Convert.ToByte(~flag);
    }

    public static bool HasFlagInMask(byte mask, byte flag)
    {
        return (mask & flag) == flag;
    }

    public static bool IsNumberIncludeInMask(byte num, byte mask)
    {
        return (num & mask) == mask;
    }
}
