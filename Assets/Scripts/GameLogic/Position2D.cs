﻿using System;

public class Position2D : IComparable<Position2D>
{
    public int R { get; set; }
    public int C { get; set; }

    public Position2D()
    {
        R = 0;
        C = 0;
    }

    public Position2D(int row, int col)
    {
        R = row;
        C = col;
    }

    public override bool Equals(object obj)
    {
        var position = obj as Position2D;
        if (position == null)
        {
            return false;
        }

        return CompareTo(position) == 0;
    }

    public int CompareTo(Position2D other)
    {
        if (C == other.C && R == other.R)
        {
            return 0;
        }
        else
        {
            return -1;
        }
    }
}