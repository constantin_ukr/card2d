﻿using System;

internal class PathFinder
{
    public bool FindWave(int startX, int startY, int targetX, int targetY, int mapHeight, int mapWidht,
            int[,] matrixMap)
        {
            int[,] cMap = CreateMap(mapHeight, mapWidht, matrixMap);
            //SimplePrintOriginalMap(mapHeight, mapWidht, cMap);
            int step = 0;
            cMap[targetY, targetX] = 0; //Начинаем с финиша
            while (true)
            {
                for (int col = 0; col < mapWidht; col++)
                {
                    for (int row = 0; row < mapHeight; row++)
                    {
                        if (cMap[row, col] == step)
                        {
                            //Ставим значение шага+1 в соседние ячейки (если они проходимы)
                            if (!IsGoBeyondGameField(col - 1, row - 1, mapWidht, mapHeight)
                                && cMap[row - 1, col] != -2
                                && cMap[row - 1, col] == -1)
                            {
                                cMap[row - 1, col] = step + 1;
                            }
                            if (!IsGoBeyondGameField(col - 1, row - 1, mapWidht, mapHeight)
                                && cMap[row, col - 1] != -2
                                && cMap[row, col - 1] == -1)
                            {
                                cMap[row, col - 1] = step + 1;
                            }
                            if (!IsGoBeyondGameField(col + 1, row + 1, mapWidht, mapHeight)
                                && cMap[row + 1, col] != -2
                                && cMap[row + 1, col] == -1)
                            {
                                cMap[row + 1, col] = step + 1;
                            }
                            if (!IsGoBeyondGameField(col + 1, row + 1, mapWidht, mapHeight)
                                && cMap[row, col + 1] != -2
                                && cMap[row, col + 1] == -1)
                            {
                                cMap[row, col + 1] = step + 1;
                            }
                        }
                    }
                }

                step++;

                if (cMap[startY, startX] != -1)
                {
#if UNITY_EDITOR
                    var textMap = $"MAP DATA: CAN{Environment.NewLine}";
                    for (int row = 0; row < mapHeight; row++)
                    {
                        for (int col = 0; col < mapWidht; col++)
                        {
                            var data = "";
                            if (startX == col && startY == row)
                            {
                                data = "*";
                            }
                            else if (targetX == col && targetY == row)
                            {
                                data = "#";
                            }
                            if (cMap[row, col] == -1)
                            {
                                data = Convert.ToString(1);
                            }
                            else if(cMap[row, col] == -2)
                            {
                                data = Convert.ToString(0);
                            }
                            else
                            {
                                data = Convert.ToString("8");
                            }
                            textMap += $"{data}, ";
                        }

                        textMap += Environment.NewLine;
                    }
                    Logger.Write(textMap);
#endif
                    return true;
                }
                if (step > matrixMap.Length)
                {
#if UNITY_EDITOR
                    var textMap = $"MAP DATA: CANNOT{Environment.NewLine}";
                    for (int row = 0; row < mapHeight; row++)
                    {
                        for (int col = 0; col < mapWidht; col++)
                        {
                            var data = "";
                            if (startX == col && startY == row)
                            {
                                data = "*";
                            }
                            else if (targetX == col && targetY == row)
                            {
                                data = "#";
                            }
                            if (cMap[row, col] == -1)
                            {
                                data = Convert.ToString(1);
                            }
                            else if(cMap[row, col] == -2)
                            {
                                data = Convert.ToString(0);
                            }
                            else
                            {
                                data = Convert.ToString("8");
                            }
                            textMap += $"{data}, ";
                        }

                        textMap += Environment.NewLine;
                    }
                    Logger.Write(textMap);
#endif
                    return false;
                }
            }
        }

    private int[,] CreateMap(int mapHeight, int mapWidht, int[,] map)
    {
        int[,] matrixMap = new int[mapHeight, mapWidht];

        for (int row = 0; row < mapHeight; row++)
        {
            for (int col = 0; col < mapWidht; col++)
            {
                // TODO: SB-10. Add indicator cross path 
                if (map[row, col] == 0)
                {
                    matrixMap[row, col] = -2; //индикатор стены
                }
                else
                {
                    matrixMap[row, col] = -1; //индикатор еще не ступали сюда
                }
            }
        }

        return matrixMap;
    }

    private static bool IsGoBeyondGameField(int x, int y, int width, int height)
    {
        return ((x == -1 || x == width) || (y == -1 || y == height));
    }
}
