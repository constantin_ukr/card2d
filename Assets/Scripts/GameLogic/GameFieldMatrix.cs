﻿using System;
using System.Collections.Generic;
using System.Linq;

public static class GameFieldMatrix
{
    private static int _width;
    private static int _height;

    private static Cell[,] _gameField;
    private static GraphData _data;

    private static List<Position2D> _startPositions;
    private static List<Position2D> _finishPositions;
    private static PathFinder _path = new PathFinder();
    private static List<Position2D> _availablePosition;
    private static List<Position2D> _probablyPositionsOfWin = new List<Position2D>();
    private static List<int> _finishingClosedCardValue;
    
    public static void CreateMatrix(int height, int width, Position2D startPosition, byte offsetX, int offsetTop, int offsetDown)
    {
        _width = width;
        _height = height;

        _gameField = new Cell[height, width];

        _startPositions = new List<Position2D>(Configuration.StartPositionCount);
        _finishPositions = new List<Position2D>(Configuration.FinishPositionCount);
        _finishingClosedCardValue = new List<int>(Configuration.FinishPositionCount);

        for (int i = 0; i < _finishingClosedCardValue.Capacity; i++)
        {
            var specialCard = CardPool.GetSpecialCardByIndex(i);
            if (specialCard == null)
            {
                break;
            }
            _finishingClosedCardValue.Add(specialCard.Id);
        }
        
        for (int row = 0; row < _height; row++)
        {
            for (int col = 0; col < _width; col++)
            {
                _gameField[row, col] = new Cell { Position = new Position2D(row, col) };
            }
        }

        _data = new GraphData(height, width);

        Vertex[] vertexes = new Vertex[]
        {
                new Vertex(PositionType.Top){IsActive = true},
                new Vertex(PositionType.Bottom){IsActive = true},
                new Vertex(PositionType.Left){IsActive = true},
                new Vertex(PositionType.Right){IsActive = true}
        };

        byte mask = CreateMaskFromVertexes(vertexes);

        Edge[] edges = new Edge[]
        {
                new Edge(vertexes[0], vertexes[1]),
                new Edge(vertexes[1], vertexes[2]),
                new Edge(vertexes[2], vertexes[3]),
                new Edge(vertexes[3], vertexes[0]),
        };

        _gameField[startPosition.R, startPosition.C] = new Cell()
        {
            IsEmpty = false, Vertices = vertexes, Edges = edges, Mask = mask, Position = startPosition
        };

        byte[,] card = CreateCardArrayFromCell(_gameField[startPosition.R, startPosition.C]);
        _data.SetData(card, startPosition);
        _startPositions.Add(new Position2D(startPosition.R, startPosition.C));

        Position2D finishPosition = new Position2D(startPosition.R, startPosition.C + offsetX);
        _gameField[finishPosition.R, finishPosition.C] = new Cell()
        {
            IsEmpty = false, Vertices = vertexes, Edges = edges, Mask = mask, Position = finishPosition
        };
        card = CreateCardArrayFromCell(_gameField[finishPosition.R, finishPosition.C]);
        _data.SetData(card, finishPosition);
        _finishPositions.Add(finishPosition);

        finishPosition = new Position2D(startPosition.R - offsetDown, startPosition.C + offsetX);
        _gameField[finishPosition.R, finishPosition.C] = new Cell()
        {
            IsEmpty = false, Vertices = vertexes, Edges = edges, Mask = mask, Position = finishPosition
        };
        card = CreateCardArrayFromCell(_gameField[finishPosition.R, finishPosition.C]);
        _data.SetData(card, finishPosition);
        _finishPositions.Add(finishPosition);

        finishPosition = new Position2D(startPosition.R + offsetTop, startPosition.C + offsetX);
        _gameField[finishPosition.R, finishPosition.C] = new Cell()
        {
            IsEmpty = false, Vertices = vertexes, Edges = edges, Mask = mask, Position = finishPosition
        };
        card = CreateCardArrayFromCell(_gameField[finishPosition.R, finishPosition.C]);
        _data.SetData(card, finishPosition);
        _finishPositions.Add(finishPosition);

        _probablyPositionsOfWin = CalculateBrobablyPositionsOfWin(_finishPositions);

        CalculateAllAvailableCells();
    }
#if UNITY_EDITOR
    public static void PrintMap()
    {
        _data.PrintMap();
    }
#endif

    public static bool IsPositionCanBeWin(Position2D position)
    {
        var result = _probablyPositionsOfWin.FirstOrDefault(pos => pos.Equals(position)) != null;
        return result;
    }
    
    public static bool GetStatusCellByCoords(Position2D targetPosition)
    {
        return _gameField[targetPosition.R, targetPosition.C].IsEmpty;
    }

    public static bool PlayCardByPosition(Vertex[] vertexes, Edge[] edges, Position2D targetPosition)
    {
        #region UNITY_EDITOR
#if UNITY_EDITOR
        var vertexesText = "VERTEX_DATA: ";
        for (int i = 0; i < vertexes.Length; i++)
        {
            vertexesText += $"{vertexes[i].PositionType} {vertexes[i].IsActive} ";
        }
        Logger.Write(vertexesText);
        var edgesText = "EDGE_DATA: ";
        for (int i = 0; i < edges.Length; i++)
        {
            edgesText += $"Id:{edges[i].Id}: {edges[i].VertexOneId.PositionType} -> {edges[i].VertexTwoId.PositionType} ";
        }
        Logger.Write(edgesText + Environment.NewLine);
#endif
        #endregion
        if (Validate(targetPosition)
            && GetStatusCellByCoords(targetPosition)
            && ValidateCellAround(vertexes, targetPosition)
            && CanCreatePath(targetPosition, vertexes, edges))
        {
            UpdateCellInPosition(vertexes, edges, targetPosition, false, true);
            
            return true;
        }
        else
        {
            return false;
        }
    }

    public static List<Tuple<int, Position2D>> OpenFinishCardsForPosition(Position2D targetPosition)
    {
        var cellData = _gameField[targetPosition.R, targetPosition.C];
        var vertices = cellData.Vertices;
        var positions = new List<Position2D>(); 
        
        for (int i = 0; i < vertices.Length; i++)
        {
            if (vertices[i].IsActive)
            {
                var tempPosition = GetPositionValue(vertices[i], targetPosition);
                if (_finishPositions.Contains(tempPosition))
                {
                    var data = cellData.Edges.FirstOrDefault(edge =>edge.VertexOneId.Equals(vertices[i]) || edge.VertexTwoId.Equals(vertices[i]));
                    if (data != null)
                    {
                        positions.Add(tempPosition);
                    }
                }
            }
        }

        if (positions.Count > 0)
        {
            var result = new List<Tuple<int, Position2D>>(positions.Count);
            for (int i = 0; i < positions.Count; i++)
            {
                var index = _finishPositions.FindIndex(position => position.Equals(positions[i]));
                
                if (index == -1)
                {
                    throw new Exception("WTF. OpenFinishCardsForPosition");
                }
                
                var cardId = _finishingClosedCardValue[index];
                
                _finishPositions.RemoveAt(index);
                _finishingClosedCardValue.RemoveAt(index);

                var cardData = CardPool.GetSpecialCardById(cardId, false) as CardBuilder;
                
                var connectedPositionType = PositionType.None;

                if (targetPosition.C - positions[i].C > 0)
                {
                    connectedPositionType = PositionType.Right;
                }
                else if (targetPosition.C - positions[i].C < 0)
                {
                    connectedPositionType = PositionType.Left;
                }
                else if (targetPosition.R - positions[i].R < 0)
                {
                    connectedPositionType = PositionType.Top;
                }
                else if (targetPosition.R - positions[i].R < 0)
                {
                    connectedPositionType = PositionType.Bottom;
                }

                var temp = cardData.Vertexes.FirstOrDefault(v => v.IsActive && v.PositionType == connectedPositionType);

                if (temp == null)
                {
                    cardData.RotateCard();
                    temp = cardData.Vertexes.FirstOrDefault(v => v.IsActive && v.PositionType == connectedPositionType);

                    if (temp == null)
                    {
                        throw new Exception("WTF OpenFinishCardsForPosition");
                    }
                }

                var cell = new Cell
                {
                    Edges = cardData.Edges,
                    IsEmpty = false,
                    Position = positions[i],
                    Vertices = cardData.Vertexes,
                    Mask = CreateMaskFromVertexes(cardData.Vertexes)
                };
                
                CreateCardArrayFromCell(cell);
                
                result.Add(new Tuple<int, Position2D>(cardId, positions[i]));
            }
            
            _probablyPositionsOfWin = CalculateBrobablyPositionsOfWin(_finishPositions);
            
            return result;
        }

        return new List<Tuple<int, Position2D>>(0);
    }
    
    public static bool PlayCardByLandslide(Position2D targetPosition)
    {
        if (Validate(targetPosition)
            && !GetStatusCellByCoords(targetPosition)
            && !DoesPositionIsImmutable(targetPosition))
        {
            Cell cell = new Cell
            {
                IsEmpty = true,
                Vertices = new Vertex[]{
                    new Vertex(PositionType.Top),
                    new Vertex(PositionType.Bottom),
                    new Vertex(PositionType.Left),
                    new Vertex(PositionType.Right)
                },
                Edges = new Edge[0],
                Position = new Position2D(targetPosition.R, targetPosition.C),
                Mask = 0
            };

            _gameField[targetPosition.R, targetPosition.C] = cell;

            UpdateCellInPosition(cell.Vertices, cell.Edges, cell.Position, true, true);

            RebuildAllAvailablePosition();
            return true;
        }
        else
        {
            return false;
        }
    }

    public static List<Position2D> GetAllAvailableCells()
    {
        return _availablePosition;
    }

    private static bool CanCreatePath(Position2D targetPosition, Vertex[] vertices, Edge[] edges)
    {
        Position2D start = new Position2D(_startPositions[0].R * 3 + 1, _startPositions[0].C * 3 + 1);
        int[,] array = new int[_height * 3, _width * 3];
        Array.Copy(_data._matrixPath, array, _data._matrixPath.Length);

        var mask = CreateMaskFromVertexes(vertices);
        var cell = new Cell { Position = targetPosition, Vertices = vertices, Mask = mask, Edges = edges, IsEmpty = false };
        var cardArray = CreateCardArrayFromCell(cell);
        var unavailablePositionTypeForCard = new List<PositionType>();
        for (int row = targetPosition.R * 3, i = 0; i < 3; row++, i++)
        {
            for (int col = targetPosition.C * 3, j = 0; j < 3; col++, j++)
            {
                array[row, col] = cardArray[i, j];
                if (cardArray[i, j] == 9)
                {
                    unavailablePositionTypeForCard.Add(GetPositionTypeFromCardArrayPosition(i, j));
                }
            }
        }

        List<Position2D> activePoints = new List<Position2D>();
        List<Position2D> unavailablePoints = new List<Position2D>();

        for (int i = 0; i < vertices.Length; i++)
        {
            if (vertices[i].IsActive)
            {
                var point = ConvertTargetPositionToPointWithPositionType(targetPosition, vertices[i].PositionType);
                activePoints.Add(point);
                if (unavailablePositionTypeForCard.Contains(vertices[i].PositionType))
                {
                    Position2D tempPoint = GetPositionValue(vertices[i], point);
                    unavailablePoints.Add(tempPoint);
                }
            }
        }
            
        var crossConnectPoints = new List<Position2D>();
        // find connect points
        for (int i = 0; i < _availablePosition.Count; i++)
        {
            for (int j = 0; j < activePoints.Count; j++)
            {
                if (_availablePosition[i].C == activePoints[j].C && _availablePosition[i].R == activePoints[j].R)
                {
                    crossConnectPoints.Add(_availablePosition[i]);
                }
            }
        }
            
        var result = false;
        for (int i = 0; i < crossConnectPoints.Count; i++)
        {
            var hasPath = _path.FindWave(start.C, start.R, crossConnectPoints[i].C, crossConnectPoints[i].R, _height * 3, _width * 3, array);
            if (hasPath)
            {
                result = true;
            }
        }

        var k = 0;
        while (k < _availablePosition.Count)
        {
            for (int i = 0, len = unavailablePoints.Count; i < len; i++)
            {
                if (_availablePosition[k].C == unavailablePoints[i].C &&
                    _availablePosition[k].R == unavailablePoints[i].R)
                {
                    _availablePosition.Remove(_availablePosition[k]);
                    unavailablePoints.Remove(unavailablePoints[i]);
                    len = unavailablePoints.Count;
                    --i;
                    --k;
                }
            }

            ++k;
        }
            
        return result;
    }

    private static void RebuildAllAvailablePosition()
    {
        Position2D start = new Position2D(_startPositions[0].R * 3 + 1, _startPositions[0].C * 3 + 1);
        var i = 0;
        while (i < _availablePosition.Count)
        {
            int[,] array = new int[_height * 3, _width * 3];
            Array.Copy(_data._matrixPath, array, _data._matrixPath.Length);
            
            var hasPath = _path.FindWave(start.C, start.R, _availablePosition[i].C, _availablePosition[i].R, _height * 3, _width * 3, array);
            if (!hasPath)
            {
                _availablePosition.Remove(_availablePosition[i]);
                --i;
            }

            ++i;
        }
        
    }

    private static Position2D GetPositionValue(Vertex vertex, Position2D position)
    {
        Position2D tempPoint = new Position2D();
        switch (vertex.PositionType)
        {
            case PositionType.Top:
                tempPoint = new Position2D(position.R - 1, position.C);
                break;
            case PositionType.Bottom:
                tempPoint = new Position2D(position.R + 1, position.C);
                break;
            case PositionType.Left:
                tempPoint = new Position2D(position.R, position.C - 1);
                break;
            case PositionType.Right:
                tempPoint = new Position2D(position.R, position.C + 1);
                break;
            default:
                throw new ArgumentOutOfRangeException();
        }

        return tempPoint;
    }

    private static PositionType GetPositionTypeFromCardArrayPosition(int rowIndex, int colIndex)
        {
            if (rowIndex == 0)
            {
                return PositionType.Top;
            }
            else if(rowIndex == 2)
            {
                return PositionType.Bottom;
            }
            else if(colIndex == 0)
            {
                return PositionType.Right;
            }
            else if (colIndex == 2)
            {
                return PositionType.Left;
            }
            
            throw new ArgumentException("not found");
        }

        private static Position2D ConvertTargetPositionToPointWithPositionType(Position2D position, PositionType positionType)
        {
            int row, col;

            switch (positionType)
            {
                case PositionType.Top:
                    row = position.R * 3;
                    col = position.C * 3 + 1;
                    break;
                case PositionType.Bottom:
                    row = position.R * 3 + 2;
                    col = position.C * 3 + 1;
                    break;
                case PositionType.Left:
                    row = position.R * 3 + 1;
                    col = position.C * 3;
                    break;
                case PositionType.Right:
                    row = position.R * 3 + 1;
                    col = position.C * 3 + 2;
                    break;
                default:
                    throw new ArgumentException("WTF a type");
            }

            var point = new Position2D(row, col);

            return point;
        }

    private static void CalculateAllAvailableCells(bool calculateAllCell = false)
    {
        var tmp = _data.GetAllAvailablePoints();
        if (calculateAllCell)
        {
            List<Position2D> notAvailablePositionsAroundFinishCell = new List<Position2D>();

            for (int j = 0; j < _finishPositions.Count; j++)
            {
                Cell fcell = _gameField[_finishPositions[j].R, _finishPositions[j].C];
                if (!ValidateCellAround(fcell.Vertices, fcell.Position))
                {
                    notAvailablePositionsAroundFinishCell.Add(new Position2D(fcell.Position.R * 3 + 3, fcell.Position.C * 3 + 1));
                    notAvailablePositionsAroundFinishCell.Add(new Position2D(fcell.Position.R * 3 - 1, fcell.Position.C * 3 + 1));
                    notAvailablePositionsAroundFinishCell.Add(new Position2D(fcell.Position.R * 3 + 1, fcell.Position.C * 3 + 3));
                    notAvailablePositionsAroundFinishCell.Add(new Position2D(fcell.Position.R * 3 + 1, fcell.Position.C * 3 - 1));
                }
            }

            for (int i = 0; i < tmp.Count; i++)
            {
                bool desreese = false;

                for (int j = 0; j < notAvailablePositionsAroundFinishCell.Count; j++)
                {
                    if (notAvailablePositionsAroundFinishCell[j].R == tmp[i].R &&
                        notAvailablePositionsAroundFinishCell[j].C == tmp[i].C)
                    {
                        tmp.Remove(tmp[i]);
                        desreese = true;
                        break;
                    }
                }

                if (desreese)
                {
                    --i;
                }
            }
        }

        _availablePosition = tmp;
    }

    private static List<Position2D> CalculateAvailablePositions(List<Position2D> points)
    {
        List<Position2D> tempAvailablePoints = new List<Position2D>(_availablePosition);
        List<Position2D> removetdPoints = new List<Position2D>();
        List<Position2D> realAvailablePointFromPoints = new List<Position2D>();

        for (int i = 0; i < tempAvailablePoints.Count; i++)
        {
            for (int j = 0; j < points.Count; j++)
            {
                if (tempAvailablePoints[i].R == points[j].R && tempAvailablePoints[i].C == points[j].C)
                {
                    removetdPoints.Add(tempAvailablePoints[i]);
                    removetdPoints.Add(points[j]);
                }
            }
        }

        tempAvailablePoints.RemoveAll(pos => removetdPoints.Contains(pos));
        points.RemoveAll(pos => removetdPoints.Contains(pos));

        for (int i = 0; i < points.Count; i++)
        {
            int row = 0;
            int col = 0;

            if (points[i].R % 3 == 0 && points[i].C % 3 == 1)
            {
                row = points[i].R - 1;
                col = points[i].C;
            }
            else if (points[i].C % 3 == 0 && points[i].R % 3 == 1)
            {
                col = points[i].C - 1;
                row = points[i].R;
            }
            else if (points[i].C % 3 == 2 && points[i].R % 3 == 1)
            {
                row = points[i].R;
                col = points[i].C + 1;
            }
            else if (points[i].R % 3 == 2 && points[i].C % 3 == 1)
            {
                row = points[i].R + 1;
                col = points[i].C;
            }
            
            realAvailablePointFromPoints.Add(new Position2D(row, col));
        }

        return realAvailablePointFromPoints;
    }

    private static bool DoesPositionIsImmutable(Position2D targetPosition)
    {
        var result = IsListContainsPosition(_startPositions, targetPosition) || IsListContainsPosition(_finishPositions, targetPosition);
        return result;
    }

    private static bool IsListContainsPosition(List<Position2D> source, Position2D targetPosition)
    {
        for (int i = 0; i < source.Count; i++)
        {
            if (source[i].R == targetPosition.R && source[i].C == targetPosition.C)
            {
                return true;
            }
        }

        return false;
    }

    private static void UpdateCellInPosition(Vertex[] vertices, Edge[] edges, Position2D targetPosition, bool isEmpty, bool mustCalculate = false)
    {
        Cell cell = _gameField[targetPosition.R, targetPosition.C];
        cell.IsEmpty = isEmpty;
        cell.Edges = edges;
        cell.Vertices = vertices;
        cell.Mask = CreateMaskFromVertexes(vertices);
        cell.Position = targetPosition;

        _data.SetData(CreateCardArrayFromCell(cell), targetPosition);
        
        if (mustCalculate)
        {
            CalculateAllAvailableCells(true);
        }
    }

    private static byte[,] CreateCardArrayFromCell(Cell cell)
    {
        Vertex[] activeVertexs = cell.Vertices.Where(v => v.IsActive).ToArray();

        bool isTop = BitMaskHelper.HasFlagInMask(cell.Mask, Convert.ToByte(PositionType.Top));
        bool isButtom = BitMaskHelper.HasFlagInMask(cell.Mask, Convert.ToByte(PositionType.Bottom));
        bool isLeft = BitMaskHelper.HasFlagInMask(cell.Mask, Convert.ToByte(PositionType.Left));
        bool isRight = BitMaskHelper.HasFlagInMask(cell.Mask, Convert.ToByte(PositionType.Right));

        byte[,] cardArray = { { 0, 0, 0 }, { 0, 0, 0 }, { 0, 0, 0 } };

        Vertex[] connected = GetConnectVectors(activeVertexs, cell.Edges);

        byte maskConnect = CreateMaskFromVertexes(connected);

        if (connected.Length == 1)
        {
            throw new Exception("not correct value");
        }

        if (connected.Length == 4)
        {
            cardArray[1, 1] = 3;
        }
        else if (connected.Length == 4 && cell.Edges.Length == 2)
        {
            cardArray[1, 1] = 4;
        }
        else if (connected.Length > 0)
        {
            cardArray[1, 1] = 2;
        }
        else if (connected.Length == 0) //landslide card
        {
            cardArray = new byte[,] { { 0, 1, 0 }, { 1, 0, 1 }, { 0, 1, 0 } };
        }

        if (isTop)
        {
            cardArray[0, 1] = Convert.ToByte(BitMaskHelper.HasFlagInMask(maskConnect, Convert.ToByte(PositionType.Top)) ? 2 : 9);
        }
        if (isButtom)
        {
            cardArray[2, 1] = Convert.ToByte(BitMaskHelper.HasFlagInMask(maskConnect, Convert.ToByte(PositionType.Bottom)) ? 2 : 9);
        }
        if (isLeft)
        {
            cardArray[1, 0] = Convert.ToByte(BitMaskHelper.HasFlagInMask(maskConnect, Convert.ToByte(PositionType.Left)) ? 2 : 9);
        }
        if (isRight)
        {
            cardArray[1, 2] = Convert.ToByte(BitMaskHelper.HasFlagInMask(maskConnect, Convert.ToByte(PositionType.Right)) ? 2 : 9);
        }

        return cardArray;
    }

    private static Vertex[] GetConnectVectors(Vertex[] vertices, Edge[] edges)
    {
        HashSet<Vertex> connectedVertices = new HashSet<Vertex>();

        if (edges == null || edges.Length == 0)
        {
            return new Vertex[0];
        }

        for (int i = 0; i < edges.Length; i++)
        {
            for (int j = 0; j < vertices.Length; j++)
            {
                if (edges[i].VertexOneId.PositionType == vertices[i].PositionType || edges[i].VertexTwoId.PositionType == vertices[i].PositionType)
                {
                    connectedVertices.Add(vertices[j]);
                }
            }
        }

        return connectedVertices.ToArray();
    }

    private static bool Validate(Position2D targetPosition)
    {
        var result = (targetPosition.C >= 0 && targetPosition.C < _width) && (targetPosition.R >= 0 && targetPosition.R < _height);
        return result;
    }

    private static bool ValidateCellAround(Vertex[] checkingCardVertexes, Position2D targetPosition) // active ancher card play 
    {
        byte activePositionPlayingCardMask = CreateMaskFromVertexes(checkingCardVertexes);
        byte[] masks = GetMasksConnectorsAroundCell(targetPosition, checkingCardVertexes);
        
        bool res = masks[0] > 0 && BitMaskHelper.IsNumberIncludeInMask(activePositionPlayingCardMask, masks[0]);
        bool res2 = masks[0] > 0 && masks[1] > 0 && BitMaskHelper.IsNumberIncludeInMask(masks[0], masks[1]);
        bool isNotActivePositionEquelsMask = masks[2] > 0 &&
            BitMaskHelper.IsNumberIncludeInMask(activePositionPlayingCardMask, masks[2]);
        return res && !res2 && !isNotActivePositionEquelsMask;
    }

    private static byte[] GetMasksConnectorsAroundCell(Position2D targetPosition, Vertex[] vertices = null)
    {
        byte mask = 0;
        byte emptyPlace = 0;
        byte mustBeEmpty = 0;
        
        Cell bottomCell = CreateFakeCellData(
            new Position2D(targetPosition.R + 1, targetPosition.C), vertices, PositionType.Bottom);
        Cell topCell = CreateFakeCellData(
            new Position2D(targetPosition.R - 1, targetPosition.C), vertices, PositionType.Top);
        Cell leftCell = CreateFakeCellData(
            new Position2D(targetPosition.R, targetPosition.C - 1), vertices, PositionType.Left);
        Cell rightCell = CreateFakeCellData(
            new Position2D(targetPosition.R, targetPosition.C + 1), vertices, PositionType.Right);
        
        /*var bottomActive = vertices.First(vertex => vertex.PositionType == PositionType.Bottom).IsActive;
        var topActive = vertices.First(vertex => vertex.PositionType == PositionType.Top).IsActive;
        var leftActive = vertices.First(vertex => vertex.PositionType == PositionType.Top).IsActive;
        var rightActive = vertices.First(vertex => vertex.PositionType == PositionType.Top).IsActive;*/
        
        ExtentionMaskForCellSide(bottomCell, PositionType.Top, /*topActive, */ref mask, ref emptyPlace, ref mustBeEmpty);
        ExtentionMaskForCellSide(topCell, PositionType.Bottom, /*bottomActive,*/ ref mask, ref emptyPlace, ref mustBeEmpty);
        ExtentionMaskForCellSide(rightCell, PositionType.Left, /*leftActive,*/ ref mask, ref emptyPlace, ref mustBeEmpty);
        ExtentionMaskForCellSide(leftCell, PositionType.Right, /*rightActive,*/ ref mask, ref emptyPlace, ref mustBeEmpty);

        return new byte[] {mask, emptyPlace, mustBeEmpty};
    }

    private static Cell CreateFakeCellData(Position2D validatedPosition, IEnumerable<Vertex> vertices, PositionType positionType)
    {
        Cell cell = null;
        
        if(Validate(validatedPosition))
        {
            cell = _gameField[validatedPosition.R, validatedPosition.C];
            if (_finishPositions.Contains(validatedPosition))
            {
                if (vertices != null && !vertices.First(vertex => vertex.PositionType == positionType).IsActive)
                {
                    cell = new Cell();
                    cell.Position = validatedPosition;
                }
            }
        }

        return cell;
    }

    private static void ExtentionMaskForCellSide(Cell cell , PositionType positionType, /*bool isCardHasActivePositioType,*/ ref byte mask, ref byte emptyPlace, ref byte mustBeEmpty)
    {
        if (cell != null && !cell.IsEmpty)
        {
            if (BitMaskHelper.HasFlagInMask(cell.Mask, Convert.ToByte(positionType)))
            {
                BitMaskHelper.AddFlagToMask(ref mask, Convert.ToByte(GetOppositePositionType(positionType)));
            }
            else
            {
                BitMaskHelper.AddFlagToMask(ref mustBeEmpty, Convert.ToByte(GetOppositePositionType(positionType)));
            }
        }
        else
        {
            BitMaskHelper.AddFlagToMask(ref emptyPlace, Convert.ToByte(GetOppositePositionType(positionType)));
        }
    }

    private static PositionType GetOppositePositionType(PositionType positionType)
    {
        switch (positionType)
        {
            case PositionType.Top:
                return PositionType.Bottom;
            case PositionType.Bottom:
                return PositionType.Top;
            case PositionType.Left:
                return PositionType.Right;
            case PositionType.Right:
                return PositionType.Left;
            default:
                throw new ArgumentOutOfRangeException("positionType", positionType, null);
        }
    }

    private static byte CreateMaskFromVertexes(Vertex[] vertexes)
    {
        byte mask = 0;

        for (int i = 0; i < vertexes.Length; i++)
        {
            if (vertexes[i].IsActive)
            {
                byte flag = Convert.ToByte(vertexes[i].PositionType);
                BitMaskHelper.AddFlagToMask(ref mask, flag);
            }
        }

        return mask;
    }
    
    private static List<Position2D> CalculateBrobablyPositionsOfWin(List<Position2D> finishPositions)
    {
        var probablyPositions = new HashSet<Position2D>();
        Vertex[] vertices = new Vertex[]
        {
            new Vertex(PositionType.Top){IsActive = true},
            new Vertex(PositionType.Bottom){IsActive = true},
            new Vertex(PositionType.Left){IsActive = true},
            new Vertex(PositionType.Right){IsActive = true}
        };
        
        for (int i = 0; i < finishPositions.Count; i++)
        {
            for (int j = 0; j < vertices.Length; j++)
            {
                var tempPosition = GetPositionValue(vertices[j], finishPositions[i]);
                if (Validate(tempPosition) && finishPositions.FirstOrDefault(pos => pos.Equals(tempPosition)) == null)
                {
                    probablyPositions.Add(tempPosition);
                }
            }
        }
        
        return probablyPositions.ToList();
    }
}
