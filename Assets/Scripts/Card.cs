﻿using UnityEngine;

public class Card : MonoBehaviour
{    
    [SerializeField] private bool _isSelected;     
    [SerializeField] private CardData _cardData;
    private int _id;
    private Transform _transform;
    private Draggable _draggable;

    public int Id
    {
        get { return _cardData.Id; }
        private set
        {
            _id = value;
            _cardData.Id = value;
        }
    }
    public CardType Type
    {
        get { return _cardData.Type; }
        private set { _cardData.Type = value; }
    }

    public bool Selected
    {
        get { return _isSelected; }
        set
        {
            if((_isSelected && value) || (!_isSelected && !value))
            {
                return;
            }

            _isSelected = value;
            if (_isSelected)
            {
                _transform.position = new Vector3(_transform.position.x, _transform.position.y + 20, 0);
                _draggable.enabled = true;
            }
            else
            {
                _draggable.enabled = false;
                _transform.position = new Vector3(_transform.position.x, _transform.position.y - 20, 0);                
            }

            print(string.Format("Id {0} status: {1}", _id, _isSelected));
        }
    }

    void Awake()
    {
        _cardData = new CardData();
        _cardData.Id = Id;
        _cardData.Type = Type;
        _isSelected = false;
        _transform = GetComponent<Transform>();
        _draggable = GetComponent<Draggable>();
        _draggable.enabled = false;
    }

    public CardData GetData()
    {
        return _cardData;
    }

    public void SetCardData(object data)
    {
        //var cardData = data as CardBuilder;

        //if (cardData != null)
        //{
        //    _cardData = cardData;
        //    Id = cardData.Id;
        //    Type = cardData.Type;
        //}
        var cardData = (CardData) data;
        _cardData = cardData;
        Id = cardData.Id;
        Type = cardData.Type;
    }
}
